#To do
# dla kazdego punktu wyznaczyc w ktora strone ma isc
# dodac wszystkie wektory.. wektory przemnozyc przez potencjaly przeszkod i celu (celu znacznie wiekszy)
# robot jezdzi po kratkach

import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch





np.set_printoptions(linewidth=np.inf)  #zmieana dlugosci liniki w numpy

delta = 0.1

wsp = 1/delta  #do macierzy Z

x_size = 10
y_size = 10

obst_vect = [(1, 1), (-3, -5), (1,-4), (-4,2)]

############################################
start_point=np.array([-10.0,-7.5])
############################################
finish_point = np.array([10.0,2.0])
############################################


x = y = np.arange(-10, 10, delta)
X, Y = np.meshgrid(x, y)

Z = np.zeros( (len(x),len(y)) )


#STALE
kp = 10    #cel
ko = 10    #przeszkody
do = 5


#obliczenia
for x_tmp in x:
     for y_tmp in y:
         robot = np.array([x_tmp,y_tmp])
         Nor = np.linalg.norm(robot - finish_point)
         Fp = kp * Nor

         Fo = 0
         for obstacle in obst_vect:   #wektor sily przeszkod
              Nor1 = np.linalg.norm(robot - obstacle)
              if Nor1 <= do:
                  Fo += (-1)*ko*( (1/Nor1) - (1/do) ) * ( 1/pow( Nor1 ,2 ))

         F = Fp + Fo

         x_tmp_int = (int) ((wsp * y_tmp) + len(x)/2)
         y_tmp_int = (int) ((wsp * x_tmp) + len(y)/2)

         if( F>0 ):
            Z[x_tmp_int][y_tmp_int] = F





##################################################################################


# rysowanie ukladu
fig = plt.figure(figsize=(10, 10))

ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size])

#rysowanie punktow
plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

#rysowanie przeszkod
for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

#################################################################################
#################################################################################

tmp_point = start_point
b = (0,0)

print("tmp poczatek= " + str(tmp_point))

#przypadek dla delty = 0.1

#tmp_point = np.array([15,-5])
i = 0
while ( ( (tmp_point[0] <= (finish_point[0] - (delta/4)) ) or (tmp_point[0] >= (finish_point[0] + (delta/4)) ) )
    or ( (tmp_point[1] <= (finish_point[1] - (delta/4)) ) or (tmp_point[1] >= (finish_point[1] + (delta/4)) ) )):


    i +=1
    print(i)
    if(i > 250):
        break



#    print("tmp= " +str(tmp_point))

    b = (0,0)
    for obstacle in obst_vect:
        Nor1 = np.linalg.norm(tmp_point - obstacle)
        Fo = (-1)*ko*( (1/Nor1) - (1/do) ) * ( 1/pow( Nor1 ,2 ))
        #Fo przyjmuje duze wartosci przy przeszkodach,
        #Fo jest ujemnie
        c = Fo *np.subtract(obstacle, tmp_point)
        b +=c

    Nor = np.linalg.norm(tmp_point - finish_point)
    Fp = kp * Nor

    c = Fp *np.subtract(finish_point, tmp_point)
    b += c # b -> wektor do punktu docelowego

    #liczenie kata
    o = np.array([1+1j])
    o.real = np.array([ b[0] ])
    o.imag = np.array([ b[1] ])
    kat = np.angle(o, deg=True)

    print("kat= " +str(kat))

    if(kat < 0):
        kat = 350 + kat

    print("kat dod= " +str(kat))

    a = np.array([tmp_point[0],tmp_point[1]])

    #przesuwanie robota
    if( (kat >= 0 and kat <= 22.5) or (kat <= 361 and kat >337.5)):
        print("3")
        tmp_point[0] += delta
    elif( kat > 22.5 and kat <= 67.5 ):
        print("2")
        tmp_point[0] += delta
        tmp_point[1] += delta
    elif( kat > 67.5 and kat <= 112.5 ):
        print("1")
        tmp_point[1] += delta
    elif( kat > 112.5 and kat <= 157.5 ):
        print("8")
        tmp_point[0] -= delta
        tmp_point[1] += delta
    elif( kat > 157.5 and kat <= 202.5 ):       #180 robi lekko w gore albo dol
        print("7")
        tmp_point[0] -= delta

        if( i%3 ):
            tmp_point[1] += delta
        # else:
        #     tmp_point[1] -= delta

    elif( kat > 202.5 and kat <= 247.5 ):
        print("6")
        tmp_point[0] -= delta
        tmp_point[1] -= delta
    elif( kat > 247.5 and kat <= 292.5 ):
        print("5")
        tmp_point[1] -= delta
    elif( kat > 292.5 and kat <= 337.5 ):
        print("4")
        tmp_point[0] += delta
        tmp_point[1] -= delta
    else:
        ("jakis dziwny ten kat")

    b = tmp_point

    print(str(tmp_point[0])+" == " + str(finish_point[0]) )
    print(str(tmp_point[1])+" == " + str(finish_point[1]) )

    #print(str(a)+" rys " + str(b) )
    plt.plot([a[0],b[0]],[a[1],b[1]]) #rysowanie lini od a do b






#rysowanie wektora

############




#rysowanie ukladu
plt.colorbar(orientation='vertical')
plt.grid(True)
plt.show()







# c)
# Przy obliczeniach najwazniejsza jest roznica pomiedzy kp i ko, im dany parametr jest wiekszy tym bardziej ten czynnik wplywa na potencjal.
# do jest zasiegiem dzialania przeszkod i w naszym przypadku elekty widac gdy jest bardzo male (~2).
