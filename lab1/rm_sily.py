import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch


np.set_printoptions(linewidth=np.inf)  #zmieana dlugosci liniki w numpy

delta = 0.1

wsp = 1/delta  #do macierzy Z

x_size = 10
y_size = 10

obst_vect = [(1, 1), (-3, -5), (1,-4), (-4,2)]

############################################
start_point=np.array([-10.0,-2.0])
############################################
finish_point = np.array([10.0,2.0])
############################################

x = y = np.arange(-10, 10, delta)
X, Y = np.meshgrid(x, y)
Z = np.zeros( (len(x),len(y)) )

#STALE
kp = 1
ko = 10    #przeszkody
do = 5

#obliczenia
for x_tmp in x:
     for y_tmp in y:
         robot = np.array([x_tmp,y_tmp])
         Nor = np.linalg.norm(robot - finish_point)
         Fp = kp * Nor

         Fo = 0
         for obstacle in obst_vect:   #wektor sily przeszkod
              Nor1 = np.linalg.norm(robot - obstacle)
              if Nor1 <= do:
                  Fo += (-1)*ko*( (1/Nor1) - (1/do) ) * ( 1/pow( Nor1 ,2 ))

         F = Fp + Fo

         x_tmp_int = (int) ((wsp * y_tmp) + len(x)/2)
         y_tmp_int = (int) ((wsp * x_tmp) + len(y)/2)

         if( F>0 ):
            Z[x_tmp_int][y_tmp_int] = F




##################################################################################


# rysowanie ukladu
fig = plt.figure(figsize=(10, 10))

ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size])

#rysowanie punktow
plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

#rysowanie przeszkod
for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')


#rysowanie ukladu
plt.colorbar(orientation='vertical')
plt.grid(True)
plt.show()
